import { Injectable } from '@angular/core';
import { Student } from '../models/student.model';
import { ClassService } from './class.service';
import { SchoolClass } from '../models/class.model';

@Injectable()
export class StudentService {
  students: Student[];
  school_class: SchoolClass;
  constructor(private classService: ClassService) { }
  getStudents(): Student[] {
    return this.students;
  }
  setSchoolClass(school_class: SchoolClass) {
    this.school_class = school_class;
  }
  updateSchoolClass() {
    this.school_class['students'] = this.students;
    const school_classes = this.classService.getClasses();
    const key = school_classes.indexOf(this.classService.getClass(this.school_class.id));
    school_classes[key] = this.school_class;
    this.classService.setClassess(school_classes);
  }
  setStudents(students: Student[]) {
    this.students = students ? students : [];
    this.updateSchoolClass();
  }
  addStudent(student: Student): void {
    let new_id: number;
    if (this.students.length > 0) {

      new_id = this.students[this.students.length - 1].id + 1;
    } else {
      new_id = 1;
    }
    student.id = new_id;
    this.students.push(student);
    this.updateSchoolClass();
  }
  getStudent(id: number): Student {
    return this.students.find(student => student.id === id);
  }
  findStudent(field: string, value: string): Student {
    return this.students.find(student => student[field] === value);
  }
  updateStudent(id: number, first_name: string, sur_name: string, age: number, gpa: number) {
    const key = this.getKey(id);
    this.students[key] = { id: id, first_name: first_name, sur_name: sur_name, age: age, gpa: gpa };
    this.updateSchoolClass();
  }
  getKey(id: number) {
    return this.students.indexOf(this.getStudent(id));
  }
  deleteStudent(id: number): Student[] {
    const student = this.getStudent(id);
    this.students = this.students.filter(obj => obj !== student);
    this.updateSchoolClass();
    return this.students;
  }
  // deleteClass(id: number): void {
  //   const school_class = this.getClass(id);
  //   this.classess = this.classess.filter(obj => obj !== school_class);
  //   // delete this.classess[scoo];
  // }
}
