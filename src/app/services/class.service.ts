import { Injectable } from '@angular/core';
import { SchoolClass } from '../models/class.model';
import { CLASSES } from '../data/class';

@Injectable()
export class ClassService {
  classes: SchoolClass[];
  constructor() {
    const classes = JSON.parse(localStorage.getItem('classes'));
    if (classes) {
      this.classes = classes;
    } else {
      this.classes = CLASSES;
    }
  }
  getClasses(): SchoolClass[] {
    return this.classes;
  }
  addClass(school_class: SchoolClass): void {
    let new_id: number;
    if (this.classes.length > 0) {

      new_id = this.classes[this.classes.length - 1].id + 1;
    } else {
      new_id = 1;
    }
    school_class.id = new_id;
    this.classes.push(school_class);
    localStorage.setItem('classes', JSON.stringify(this.classes));
  }
  getClass(id: number): SchoolClass {
    return this.classes.find(schoolClass => schoolClass.id === id);
  }
  deleteClass(id: number): void {
    const school_class = this.getClass(id);
    this.classes = this.classes.filter(obj => obj !== school_class);
    localStorage.setItem('classes', JSON.stringify(this.classes));
  }
  setClassess(classes: SchoolClass[]) {
    this.classes = classes;
    localStorage.setItem('classes', JSON.stringify(this.classes));
  }
}
