import { Student } from '../models/student.model';
import { SchoolClass } from '../models/class.model';
import { STUDENTS } from './student';

export const CLASSES: SchoolClass[] = [
  { id: 1, name: 'Biology', location: 'Room 201', teacher: 'Mr Roberson', students: STUDENTS},
  { id: 2, name: 'English', location: 'Room 302', teacher: 'Mr Statham', students: null}

];
