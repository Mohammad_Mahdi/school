import { Student } from '../models/student.model';

export const STUDENTS: Student[] = [
  { id: 1, first_name: 'David', sur_name: 'Jackson', age: 18, gpa: 3.5},
  { id: 2, first_name: 'Peter', sur_name: 'Parker', age: 17, gpa: 4.5},
  { id: 3, first_name: 'Robert', sur_name: 'White', age: 19, gpa: 2},
  { id: 4, first_name: 'Smith', sur_name: 'Black', age: 20, gpa: 3},

];
