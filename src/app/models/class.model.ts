import {Student} from './student.model';
// import {Deserializable} from './deserializable.model";

export class SchoolClass {
  id: number;
  name: string;
  location: string;
  teacher: string;
  students: Student[];


  // deserialize(input: any): Student {
  //   Object.assign(this, input);
  //   this.student = new Student().deserialize(input.car);
  //   return this;
  // }
}
