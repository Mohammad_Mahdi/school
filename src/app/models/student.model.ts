// import {Deserializable} from './deserializable.model';

export class Student   {
  id: number;
  first_name: string;
  sur_name: string;
  age: number;
  gpa: number;

  // deserialize(input: any): this {
  //   Object.assign(this, input);
  //   return this;
  // }
}
