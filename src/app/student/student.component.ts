import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { SchoolClass } from '../models/class.model';
import { Student } from '../models/student.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StudentService } from '../services/student.service';
import { ClassService } from '../services/class.service';
import { NullInjector } from '@angular/core/src/di/injector';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit, OnChanges {
  error: string = null;
  @Input() school_class: SchoolClass;
  students: Student[];
  student: Student;
  formSubmitted = false;
  editForm: FormGroup;
  selected_student: Student;

  constructor(private studentService: StudentService, private classService: ClassService) { }

  ngOnInit() {
    this.studentService.setSchoolClass(this.school_class);
    this.studentService.setStudents(this.school_class.students);
    this.students = this.studentService.getStudents();
  }
  ngOnChanges() {
    this.studentService.setSchoolClass(this.school_class);
    this.studentService.setStudents(this.school_class.students);
    this.students = this.studentService.getStudents();
  }
  openStudentEditForm(id: number) {
    console.log(id);
    this.formSubmitted = false;
    this.student = this.studentService.getStudent(id);
    this.editForm = new FormGroup({
      'first_name': new FormControl(this.student.first_name, [Validators.required]),
      'sur_name': new FormControl(this.student.sur_name, [Validators.required]),
      'age': new FormControl(this.student.age, [Validators.required]),
      'gpa': new FormControl(this.student.gpa, [Validators.required]),
    });
  }
  openStudentAddForm() {
    this.formSubmitted = false;
    this.student = null;
    this.editForm = new FormGroup({
      'first_name': new FormControl(null, [Validators.required]),
      'sur_name': new FormControl(null, [Validators.required, this.checkDuplicate.bind(this)]),
      'age': new FormControl(null, [Validators.required]),
      'gpa': new FormControl(null, [Validators.required]),
    });
  }
  checkDuplicate(control: FormControl) {

    if (this.studentService.findStudent('sur_name', control.value)) {
      return { 'duplicate': true };
    }
  }
  onEditStudentSubmit(id: number) {
    const params = this.editForm.controls;
    this.studentService.updateStudent(id, params.first_name.value, params.sur_name.value, params.age.value, params.gpa.value);
    this.formSubmitted = true;
  }
  onAddStudentSubmit() {
    const params = this.editForm.controls;
    const student: Student = {
      id: null,
      first_name: params.first_name.value,
      sur_name: params.sur_name.value,
      age: params.age.value,
      gpa: params.gpa.value
    };
    this.studentService.addStudent(student);
    this.formSubmitted = true;
  }
  onDeleteStudent(id: number) {
    this.students = this.studentService.deleteStudent(id);
  }
  // editSurName(sur_name: string): void {
  //     if ( this.students.find(student => student.sur_name === sur_name) ) {
  //         this.error = sur_name + ' was exist already!';
  //         return false;
  //   }
  // }

}
