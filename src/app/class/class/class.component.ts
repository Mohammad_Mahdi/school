import { Component, OnInit } from '@angular/core';
import { ClassService } from 'src/app/services/class.service';
import { SchoolClass } from 'src/app/models/class.model';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {
  addClassForm: FormGroup;

  constructor(private classService: ClassService) { }
  school_classess: SchoolClass[] = [];
  selected_class: SchoolClass = null;
  ngOnInit() {
    this.school_classess = this.classService.getClasses();
  }
  trackByIndex(index: number, obj: any): any {
    return index;
  }
  edit(id: number) {
    this.selected_class = this.classService.getClass(id);
  }
  delete(id: number) {
    this.classService.deleteClass(id);
  }
  openClassAddForm() {
    this.addClassForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'location': new FormControl(null, [Validators.required]),
      'teacher': new FormControl(null, [Validators.required]),
    });
  }
  onAddClassSubmit() {
    const params = this.addClassForm.controls;
    const school_class: SchoolClass = {
      id: null,
      name: params.name.value,
      location: params.location.value,
      teacher: params.teacher.value,
      students: [],
    };
    this.classService.addClass(school_class);
  }
}
