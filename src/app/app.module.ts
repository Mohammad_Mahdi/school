import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClassComponent } from './class/class/class.component';
import { ClassService } from './services/class.service';
import { StudentComponent } from './student/student.component';
import { StudentService } from './services/student.service';

@NgModule({
  declarations: [
    AppComponent,
    ClassComponent,
    StudentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ClassService,StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
